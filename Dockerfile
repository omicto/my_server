FROM node
MAINTAINER omicto
ENV HOME /root
RUN git clone https://gitlab.com/omicto/my_server.git
WORKDIR my_server
CMD node server.js
EXPOSE 1111
